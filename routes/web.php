<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;


Route::group(['middleware' => 'admin'], function () {
    Route::get('/users', 'AdminController@users')->name('users');
    Route::get('/admin-products', 'AdminController@products')->name('admin.products');
    Route::post('/add-products', 'AdminController@addProduct')->name('admin.add.product');
    Route::post('/edit-product/update/{id}', 'AdminController@updateProduct')->name('admin.update.product');
    Route::get('/delete-product/{id}', 'AdminController@deleteProduct')->name('admin.delete.product');
    Route::get('/edit-product/{id}', 'AdminController@editProduct')->name('admin.edit.product');
});

Route::get('/', 'HomeController@index')->name("home");
Route::get('/products', 'HomeController@products')->name("home.products");
Route::get('/about', 'HomeController@index')->name("about");
Route::get('/contact', 'ContactController@index')->name("contact");

Route::get('/logout', function(){
    Auth::logout();
    return redirect('/');
})->name("logout");

