<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Models\User;
use App\Models\Products;
use App\Models\Images;

class AdminController extends Controller
{
    public function users(){
        $users = User::where('role', User::USER)->get();

        return view("admin.users", compact('users'));
    }

    public function products(){

        $products = Products::with('images')->get();

        foreach($products as $product){
            $product['iamges'] = $product->images;
        }

        return view("admin.products", compact('products'));
    }

    public function addProduct(ProductRequest $request){
        $product = new Products;
        $product->title = $request->post('title');
        $product->description = $request->post('description');
        $product->size = $request->post('size');
        $product->color = $request->post('color');
        $product->quantity = $request->post('quantity');
        $product->price = $request->post('price');
        $product->save();
        $productId = $product->id; 

        foreach($request->file("images") as $image){
            $imageName = time().rand(1,100).'.'.$image->extension();  
    
            $image->move(public_path('img/products'), $imageName);

            $imageInsert = new Images;
            $imageInsert->product_id = $productId;
            $imageInsert->path = 'img/products/'.$imageName;
            $imageInsert->timestamps = false;
            $imageInsert->save();
        }

        return back()->with('success','Product added!.');
    }

    public function editProduct(Request $request){

        $products = Products::with('images')->where('id', $request->route('id'))->get();
        $product = $products[0];

        return view("admin.edit_product", compact('product'));
    }

    public function updateProduct(Request $request){
        $data = $request->all();
        $token = array_shift($data);

        foreach($data as $key => $item){
            Products::where('id', $request->route('id'))->update([$key => $item]);
        }

        return back()->with('success','Product edited!.');
    }

    public function deleteProduct(Request $request){
        Products::where('id', $request->route('id'))->delete();

        
        return back()->with('success','Product deleted!.');
    }

}
