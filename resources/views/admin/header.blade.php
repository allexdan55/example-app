<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" href="{{ asset("css/admin_menu.css") }}">
    <title>@yield('title')</title>
</head>
<body id="body-pd">
        <header class="header" id="header">
            <div class="header_toggle"> <i class='bx bx-menu' id="header-toggle"></i> </div>
        </header>
        <div class="l-navbar" id="nav-bar">
            <nav class="nav">
                <div>
                    <a href="#" class="nav_logo">
                        <img src="{{ asset('img/logo.svg') }}" alt="">
                        <span class="nav_logo-name">{{ __("labels.app") }}</span>
                    </a>
                    <div class="nav_list"> 
                        <a href="{{ route("home") }}" class="nav_link {{ Route::is("home") ? "active" : "" }}"> 
                            <i class="fas fa-tachometer-alt"></i> 
                            <span class="nav_name">Dashboard</span> 
                        </a> 
                        <a href="{{ route("users") }}" class="nav_link {{ Route::is("users") ? "active" : "" }}"> 
                            <i class='bx bx-user nav_icon'></i> 
                            <span class="nav_name">Users</span> 
                        </a> 
                        <a href="#" class="nav_link {{ Route::is("orders") ? "active" : "" }}"> 
                            <i class="fas fa-dolly"></i> 
                            <span class="nav_name">Orders</span> 
                        </a> 
                        <a href="{{ route("admin.products") }}" class="nav_link {{ Route::is("admin.products") ? "active" : "" }}"> 
                            <i class="fas fa-ring"></i>
                            <span class="nav_name">Products</span> 
                        </a> 
                        <a href="#" class="nav_link"> 
                            <i class='bx bx-folder nav_icon'></i> 
                            <span class="nav_name">Files</span> 
                        </a> 
                        <a href="#" class="nav_link"> 
                            <i class='bx bx-bar-chart-alt-2 nav_icon'></i> 
                            <span class="nav_name">Stats</span> 
                        </a> 
                    </div>
                </div> <a data-url="{{ route('logout') }}" class="nav_link logout-button"> <i class='bx bx-log-out nav_icon'></i> <span class="nav_name">SignOut</span> </a>
            </nav>
        </div>
        <!--Container Main start-->
        <div class="height-100 bg-light">
            @yield('content')
            
        </div>
        <!--Container Main end-->

     
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="{{ asset("js/general.js") }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="{{ asset("js/admin.js") }}"></script>
</body>