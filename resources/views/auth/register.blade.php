@extends('auth.auth')
@section('title', 'Register')
    

@section('content')
<a href="{{ route("home") }}" class="back-button"><i class="fas fa-arrow-left"></i> {{ __('labels.menu.back') }}</a>
<div class="container">
    <div class="login-form">
        <h2 class="title">
            <div class="card-header"><img src="{{ asset("img/logo.svg") }}" alt="Logo">{{ __('labels.app') }}</div>
        </h2>
        
        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div class="input-field">
                <label for="name">{{ __('Name') }}</label>
                    <input id="name" type="text" class=" @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
            </div>

            <div class="input-field">
                <label for="email">{{ __('E-Mail Address') }}</label> 
                <input id="email" type="email" class=" @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="input-field">
                <label for="pass">{{ __('Password') }}</label>
                <input id="password" type="password" class=" @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="input-field">
                <label for="password-confirm">{{ __('Confirm Password') }}</label>
                <input id="password-confirm" type="password" name="password_confirmation" required autocomplete="new-password">

            </div>
  
      <p class="terms">By Continue, you agree our <strong>Condition of use</strong> and <strong>Privacy Notice</strong>.</p>
  
      <button type="submit" class="btn submit">{{ __("register") }}</button>
  
        </form>
      <div class="new-user text-center">
        <p>Have an account already?</p>
        <a href="{{ route('login') }}" class="btn redirect">Login</a>
  
      </div>
    </div>
</div>
@endsection