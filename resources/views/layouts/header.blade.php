<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{  asset("img/logo.svg") }}"">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css">
    <link href="https://fonts.googleapis.com/css2?family=Varela+Round&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <title>@yield('title')</title>
</head>
<body>
    <nav class="d-flex justify-content-between" id="nav">
        <div class="logo" ><img src="{{ asset("img/logo.svg") }}" alt="">Lavish Cult</div>
        <ul class="menu ">
            <li><a href="{{ route("home") }}" class="current" data-hover="{{ __("labels.menu.home") }}">{{ __("labels.menu.home") }}</a></li>
            <li><a href="{{ route("home.products") }}" data-hover="{{ __("labels.menu.products") }}">{{ __("labels.menu.products") }}</a></li>
            <li><a href="{{ route("about") }}" data-hover="{{ __("labels.menu.about") }}">{{ __("labels.menu.about") }}</a></li>
            <li><a href="{{ route("contact") }}" data-hover="{{ __("labels.menu.contact") }}">{{ __("labels.menu.contact") }}</a></li>
        </ul>
        <div class="d-flex account justify-content-end">
            @if (auth()->user())
                <a href=""><i class="fas fa-shopping-cart"></i>{{ __("labels.menu.cart") }}</a>
                <span> | </span>
                <a class="logout-button"  data-url="{{ route("logout") }}"><i class="fas fa-sign-out-alt"></i>{{ __("labels.menu.logout") }}</a>
            @else 
                <a href="{{ route("login") }}"><i class="fas fa-user"></i>{{ __("labels.menu.login") }}</a>
                <span> | </span>
                <a href="{{ route("register") }}"><i class="fas fa-user-plus"></i>{{ __("labels.menu.register") }}</a>
            @endif
            
        </div>
    </nav>


    @yield('content')

    <div class="scrollTop"><i class="fas fa-arrow-up"></i></div>    
    <footer>

    </footer>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset("js/general.js") }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>