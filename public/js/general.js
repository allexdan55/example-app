$(".scrollTop").hide();

$(window).scroll(function() {
    let height = $(window).scrollTop();
    if(height  > 70) {
        $("nav").css('background-color', 'rgba(41, 41, 41, 0.7)');
        $("nav").css('border-bottom', '2px solid #000');
        $(".scrollTop").show();
    }else{
        $("nav").css('background-color', 'transparent');
        $("nav").css('border-bottom', 'none');
        $(".scrollTop").hide();
    }
});

$(".scrollTop").on('click', function(){
    $(window).scrollTop(0);
});

$(".arrow").on('click', function(){
    $(window).scrollTop(1000);
})


$(".logout-button").on("click", function(){
    Swal.fire({
        title: 'Are you sure?',
        text: "Do you want to logout?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, logout!'
      }).then((result) => {
        if (result.isConfirmed) {
            window.location.href = $(this).data("url");
        }
      })
});