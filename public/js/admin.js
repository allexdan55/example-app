document.addEventListener("DOMContentLoaded", function(event) {

    const showNavbar = (toggleId, navId, bodyId, headerId) =>{
    const toggle = document.getElementById(toggleId),
    nav = document.getElementById(navId),
    bodypd = document.getElementById(bodyId),
    headerpd = document.getElementById(headerId)
    
    // Validate that all variables exist
    if(toggle && nav && bodypd && headerpd){
    toggle.addEventListener('click', ()=>{
    // show navbar
    nav.classList.toggle('width')
    // change icon
    toggle.classList.toggle('bx-x')
    // add padding to body
    bodypd.classList.toggle('body-pd')
    // add padding to header
    headerpd.classList.toggle('body-pd')
    })
    }
    }
    
    showNavbar('header-toggle','nav-bar','body-pd','header')
    
    
    });
    
    $(document).ready( function () {
        $('#users').DataTable();
    } );

    $(document).ready( function () {
        $('#products').DataTable();
    } );


    $('.test-popup-link').magnificPopup({
        type: 'image'
        // other options
      });

/* ----------------------Add product Image js---------------------------- */
window.onload = function(){   

  if(window.File && window.FileList && window.FileReader)
  {
      $('#files').on("change", function(event) {
          var files = event.target.files; //FileList object
          var output = document.getElementById("result");
          for(var i = 0; i< files.length; i++)
          {
              var file = files[i];
              //Only pics
              // if(!file.type.match('image'))
              if(file.type.match('image.*')){
                  if(this.files[0].size < 2097152){    
                // continue;
                  var picReader = new FileReader();
                  picReader.addEventListener("load",function(event){
                      var picFile = event.target;
                      var div = document.createElement("div");
                      div.innerHTML = "<img class='thumbnail' class='w-50' src='" + picFile.result + "'" +
                              "title='preview image'/>";
                      output.insertBefore(div,null);            
                  });
                  //Read the image
                  $('#clear, #result').show();
                  picReader.readAsDataURL(file);
                  }else{
                      alert("Image Size is too big. Minimum size is 2MB.");
                      $(this).val("");
                  }
              }else{
              alert("You can only upload image file.");
              $(this).val("");
          }
          }                               
         
      });
  }
  else
  {
      console.log("Your browser does not support File API");
  }
}

 $('#files').on("click", function() {
      $('.thumbnail').parent().remove();
      $('result').hide();
      $(this).val("");
  });

  $('#clear').on("click", function() {
      $('.thumbnail').parent().remove();
      $('#result').hide();
      $('#files').val("");
      $(this).hide();
  });

