<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Images;

class Products extends Model
{
    use HasFactory;

    const BLACK = 1;
    const SILVER = 2;
    const BLUE = 3;
    const RED = 4;
    const GREEN = 5;
    const GOLD = 6;
    const MULTI = 7;

    const COLORS = [
        self::BLACK => 'black',
        self::SILVER => 'silver',
        self::BLUE => 'blue',
        self::RED => 'red',
        self::GREEN => 'green',
        self::GOLD => 'gold',
        self::MULTI => 'multi',
    ];

    const SIZE = [9, 10, 11];


    public $table = 'products';

    protected $fillable = [
        'title',
        'description',
        'size',
        'color',
        'quantity',
        'price',
    ];


    public function images()
    {
        return $this->hasMany(Images::class, 'product_id');
    }
}
