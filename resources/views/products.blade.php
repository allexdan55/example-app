@extends('layouts.header')
@section('title', "Lavish Cult-Products")
    
@section('content')
{{-- <div class="menu_cover"></div> --}}
<div class="container">
    <section class="section_products">
        <h3 class="text-center">Rings</h3>

        <div class="d-flex ">
        @foreach ($products as $product)
            <div class="card">
                <div class="image"><img src="{{ $product->images[0]->path }}" alt=""></div>
                <p class="title">{{ $product->title }}</p>
                <p class="price">{{ $product->price }} Lei</p>
            </div>
        @endforeach
        </div>

    </section>
</div>
@endsection