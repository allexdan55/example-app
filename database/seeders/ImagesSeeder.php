<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('images')->insert([
            [
                'product_id' => 1,
                'path' => 'img/products/01.jpg',
            ],
            [
                'product_id' => 1,
                'path' => 'img/products/02.jpg',
            ],
            [
                'product_id' => 2,
                'path' => 'img/products/03.jpg',
            ]
        ]);
    }
}
