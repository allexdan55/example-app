@extends('admin.header')

@section('content')
    
<div class="row m-t-25 stats">
    <div class="col-sm-6 col-lg-3 panel">
        <h4 class="stats-title">Total users</h4>
        <span class="stats-icon"><i class="fas fa-users"></i></span>
        <h1 class="stats-number">{{ $stats['totalUsers'] }}</h1>
        
    </div>
    <div class="col-sm-6 col-lg-3 panel">
        <h4 class="stats-title">New users</h4>
        <span class="stats-icon"><i class="fas fa-chart-area"></i></span>
        <h1 class="stats-number">{{ $stats['newUsers'] }}</h1>
    </div>
    <div class="col-sm-6 col-lg-3 panel">
        <h4 class="stats-title">Orders</h4>
        <span class="stats-icon"><i class="fas fa-dolly"></i></span>
        <h1 class="stats-number">67</h1>
    </div>
    <div class="col-sm-6 col-lg-3 panel">
        <h4 class="stats-title">Income</h4>
        <span class="stats-icon"><i class="fas fa-wallet"></i></span>
        <h1 class="stats-number">123 Lei</h1>
    </div>
</div>

@endsection