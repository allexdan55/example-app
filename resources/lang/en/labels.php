<?php
return [
    "app"  => "Lavish Cult",
    "menu" => [
        "home"      => "Home",
        "products"  => "Shop",
        "about"     => "About us",
        "contact"   => "Contact",
        "cart"      => "Cart",
        "login"     => "Login",
        "logout"    => "Logout",
        "register"  => "Register",
        "back"      => "Back"
    ],
    "colors" => [
        'black' => 'Black',
        'silver' => 'Silver',
        'blue' => 'Blue',
        'red' => 'Red',
        'green' => 'Green',
        'gold' => 'Gold',
        'multi' => 'Multi',
    ]
];