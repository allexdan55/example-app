@extends('layouts.header')

@section('title', 'Contact')

@section('content')
    
{{-- <div class="menu_cover"></div> --}}
    <section class="section_contact_form">
        <h3 class="text-center">If you have any questions, contact us!</h3>
        <form class="contact_form" action="" method="POST">
            @csrf
            <div class="input-field">
                <label for="Name">Name:</label> 
                <input id="Name" type="Name" class=" @error('Name') is-invalid @enderror" name="Name" value="{{ old('Name') }}" required autocomplete="Name" autofocus>
                @error('Name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="input-field">
                <label for="email">{{ __('E-Mail Address') }}:</label> 
                <input id="email" type="email" class=" @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="input-field">
                <label for="message">Message:</label> 
                <textarea name="message" id="message" rows="5"></textarea>
            </div>
            <button type="submit" class="button contact_button btn btn-info">Send!</button>
        </form>
    </section>

@endsection