@extends('auth.auth')

@section('title', 'Login')
    
@section('content')
<a href="{{ route("home") }}" class="back-button"><i class="fas fa-arrow-left"></i> {{ __('labels.menu.back') }}</a>
<div class="container">
    <div class="login-form">
        <h2 class="title">
            <div class="card-header"><img src="{{ asset("img/logo.svg") }}" alt="Logo">{{ __('labels.app') }}</div>
        </h2>
        
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="input-field">
                <label for="email">{{ __('E-Mail Address') }}</label> 
                <input id="email" type="email" class=" @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="input-field">
                <label for="pass">{{ __('Password') }}</label>
                <input id="password" type="password" class=" @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="input">
                <div>
                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <label for="remember">{{ __('Remember Me') }}</label>
                </div>
                <a href="#" class="forgot-password">forgot your password?</a>
            </div>
  
      <button type="submit" class="btn submit">{{ __("Login") }}</button>
  
        </form>
      <div class="new-user text-center">
        <p>New on our platform?</p>
        <a href="{{ route('register') }}" class="btn redirect">Create account</a>
  
      </div>
    </div>
</div>
@endsection