<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'title' => 'Inel Rotativ',
                'description' => 'se invarte foarte frumos',
                'size' => 10,
                'color' => 1,
                'quantity' => 10,
                'price' => 30,
            ],
            [
                'title' => 'Inel Rotativ',
                'description' => 'se invarte foarte frumos',
                'size' => 9,
                'color' => 1,
                'quantity' => 10,
                'price' => 30,
            ],
            [
                'title' => 'Inel Rotativ',
                'description' => 'se invarte foarte frumos',
                'size' => 11,
                'color' => 2,
                'quantity' => 10,
                'price' => 30,
            ]
        ]);
    }
    
}
