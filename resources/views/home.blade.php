@extends('layouts.header')
@section('title', "Lavish Cult")
    
@section('content')
    <div class="background_shadow">
    <section class="background">
        <div class="arrow">
            <div class="tip1"></div>
            <div class="tip2"></div>
        </div>
    </section>
    </div>
    <section class="tabs d-flex justify-content-between">
        <div class="tab">
            <div class="icon"><i class="fas fa-ring"></i></div>
            <p>Products in stock</p>
            <p class="text">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi rerum commodi porro accusamus quas non ex mollitia, accusantium ut omnis!</p>
        </div>
        <div class="tab">
            <div class="icon"><i class="fas fa-shuttle-van"></i></div>
            <p>Free shipping</p>
            <p class="text">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi rerum commodi porro accusamus quas non ex mollitia, accusantium ut omnis!</p>
        </div>
        <div class="tab">
            <div class="icon"><i class="fas fa-brush"></i></div>
            <p>Custom rings</p>
            <p class="text">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi rerum commodi porro accusamus quas non ex mollitia, accusantium ut omnis!</p>
        </div>
    </section>
@endsection