<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => "Alex",
                'email' => "alex@lavishcult.com",
                'password' => Hash::make('!Q@w3e4r5t6y'),
                'role' => 1,
            ],
            [
                'name' => "Florin",
                'email' => "florin@lavishcult.com",
                'password' => Hash::make('!Q@w3e4r5t6y'),
                'role' => 1,
            ],
            [
                'name' => "Steve",
                'email' => "steve@lavishcult.com",
                'password' => Hash::make('!Q@w3e4r5t6y'),
                'role' => 1,
            ]
        ]);
    }
}
