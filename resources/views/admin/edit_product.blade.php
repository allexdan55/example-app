@extends('admin.header')

@section('title', "Edit Product")

@section('content')

<a href="{{ route("admin.products") }}"><button class="btn btn-info">Back</button></a>
<h1 class="mb-3">Product id: {{ $product->id }}</h1>
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
                <strong>{{ $message }}</strong>
        </div>
    @endif

<form action="{{ route("admin.update.product", ['id' => $product->id]) }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row mb-3">
    <div class="col-md-3">Title:</div>
    <div class="col-md-9 ml-auto"><input type="text" class="form-control" placeholder="Title" value="{{ $product->title }}" name="title"></div>
    </div>

    <div class="row mb-3">
        <div class="col-md-3">Description:</div>
        <div class="col-md-9 ml-auto"><input type="text" class="form-control" placeholder="Description" value="{{ $product->description }}" name="description"></div>
    </div>

    <div class="row mb-3">
        <div class="col-md-3">Size:</div>
        <div class="col-md-9 ml-auto">
            <select name="size" class="form-control">
                @foreach (\App\Models\Products::SIZE as $size)
                    <option {{ $product->size == $size ? "selected": "" }} value="{{ $size }}">{{ $size }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-md-3">Color:</div>
        <div class="col-md-9 ml-auto">
            <select name="color" class="form-control">
                @foreach (\App\Models\Products::COLORS as $id => $color)
                    <option {{ $product->color == $id ? "selected": "" }} value="{{ $id }}">{{ __("labels.colors.".$color) }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-md-3">Quantity:</div>
        <div class="col-md-9 ml-auto"><input type="number" class="form-control" value="{{ $product->quantity }}" placeholder="Quantity" min="1" name="quantity"></div>
    </div>

    <div class="row mb-3">
        <div class="col-md-3">Price:</div>
        <div class="col-md-9 ml-auto"><input type="number" class="form-control" value="{{ $product->price }}" placeholder="Price" min="1" name="price"></div>
    </div>

    {{-- @foreach ($product->images as $image)
    <div class="row mb-3">
        <a class="test-popup-link" href="{{ $image->path }}"><img src="{{ $image->path }}" class="table_image" alt="{{ $image->path }}"></a>
    </div>
    @endforeach --}}
    
    <button type="submit" class="btn btn-info">Update</button>
    
</form>

@endsection