<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Products;
use Config;

class HomeController extends Controller
{
    public function index(){
        if (Auth::user()){
            if(Auth::user()->role == Config::get('constants.global_constants.ADMIN')){
                $stats = [
                    "totalUsers" => User::where('role', User::USER)->count(),
                    "newUsers"   => User::whereMonth('created_at', now()->month)->count()
                ];
                return view('admin.admin', compact("stats"));
            }
        }
        return view("home");
    }

    public function products(){
        $products = Products::with('images')->get();
        // dd($products);
        

        return view("products", compact('products'));
    }
}
