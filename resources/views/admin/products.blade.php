@extends('admin.header')

@section('title', "Products")

@section('content')
    <h1>Products</h1>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
                <strong>{{ $message }}</strong>
        </div>
    @endif
    <button type="button" id="add_product_button" class="btn btn-success mb-4" data-bs-toggle="modal" data-bs-target="#add_product">Add product</button>
    <table id="products" class="display">
        <thead>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Description</th>
                <th>Size</th>
                <th>Color</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Images</th>
                <td>Options</td>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)
                <tr>
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->title }}</td>
                    <td>{{ $product->description }}</td>
                    <td>{{ $product->size }}</td>
                    <td>
                        <div class="item-color {{ \App\Models\Products::COLORS[$product->color] }}">
                            {{ __("labels.colors.".\App\Models\Products::COLORS[$product->color]) }}
                        </div>
                    </td>
                    <td>{{ $product->quantity }}</td>
                    <td>{{ $product->price }}</td>
                    <td>
                    @foreach ($product['images'] as $image)
                        <a class="test-popup-link" href="{{ $image->path }}"><img src="{{ $image->path }}" class="table_image" alt=""></a>
                    @endforeach    
                    </td>
                    <td>
                        <a href="{{ route("admin.edit.product", ['id' => $product->id]) }}"><button class="btn option-item"><i class="fas fa-edit"></i></button></a>
                        <a href="{{ route("admin.delete.product", ['id' => $product->id]) }}"><button class="btn btn-danger "><i class="fas fa-trash-alt"></i></button></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>


<!-- Modal -->
<div class="modal fade" id="add_product" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="title">Adauga Produs</h5>
          <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <form action="{{ route("admin.add.product") }}" method="POST" enctype="multipart/form-data">
                    @csrf
                <div class="row mb-3">
                  <div class="col-md-3">Title:</div>
                  <div class="col-md-9 ml-auto"><input type="text" class="form-control" placeholder="Title" name="title"></div>
                </div>

                <div class="row mb-3">
                    <div class="col-md-3">Description:</div>
                    <div class="col-md-9 ml-auto"><input type="text" class="form-control" placeholder="Description" name="description"></div>
                </div>

                <div class="row mb-3">
                    <div class="col-md-3">Size:</div>
                    <div class="col-md-9 ml-auto">
                        <select name="size" class="form-control">
                            @foreach (\App\Models\Products::SIZE as $size)
                            <option value="{{ $size }}">{{ $size }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-md-3">Color:</div>
                    <div class="col-md-9 ml-auto">
                        <select name="color" class="form-control">
                            @foreach (\App\Models\Products::COLORS as $id => $color)
                                <option value="{{ $id }}">{{ __("labels.colors.".$color) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-md-3">Quantity:</div>
                    <div class="col-md-9 ml-auto"><input type="number" class="form-control" placeholder="Quantity" min="1" name="quantity"></div>
                </div>

                <div class="row mb-3">
                    <div class="col-md-3">Price:</div>
                    <div class="col-md-9 ml-auto"><input type="number" class="form-control" placeholder="Price" min="1" name="price"></div>
                </div>

                <div class="multi-upload">
                    <label for="files" class="mt-3 mb-3 btn btn-info" />Choose images:</label>
                    <input id="files" type="file" name="images[]" accept="image/*" multiple="true" multiple/>
                    <button type="button" class="btn btn-danger" id="clear">Clear</button>
                                                 <output class="mt-3 mb-3" id="result" />
                </div>
        
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-info">Add</button>
        </div>
        </form>
      </div>
    </div>
  </div>


@endsection
    